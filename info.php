<?php
/**
 * WBCE CMS
 * Way Better Content Editing.
 * Visit https://wbce.org to learn more and to join the community.
 *
 * @copyright Ryan Djurovich (2004-2009)
 * @copyright WebsiteBaker Org. e.V. (2009-2015)
 * @copyright WBCE Project (2015-)
 * @license GNU GPL2 (or any later version)
 */

$module_directory = 'znip_merkzettel';
$module_name = 'znip Merkzettel';
$module_function = 'page';
$module_version = '0.2.1';
$module_platform = '2.8.2';
$module_author = 'Chio Maisriml, Bernd Michna';
$module_license = 'GNU General Public License';
$module_description = 'Adds a Wish List (Merkzettel) function to all specified elements.';
?>
