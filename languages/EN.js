var mmz_lang = {

	'add': 'Auf den Merkzettel',
	'remove': 'Vom Merkzettel entfernen',
	'remove_all': 'Alle entfernen',
	'remove_confirm': 'Wirklich alle vom Merkzettel entfernen?',
	'dein_merkzettel': 'Ihr Merkzettel',
	'mein_merkzettel': 'Mein Merkzettel:',
	'dein_merkzettel_leer': 'Der Merkzettel ist noch leer',
	
	'tdmenge': 'Menge',
	'tdtitel': 'Bezeichnung',
	'tdstk': 'Stk',
	'tdsum': 'Sum',
	'tdsumtitle': 'Gesamt inkl 20% USt:',
	
	'goto_form': 'Anfrage',
	
	'general_hint': '<h4>Dies ist <strong>kein</strong> Onlineshop! </h4><p>Nach Ihrer unverbindlichen Anfrage melden wir uns umgehend mit einem verbindlichen Angebot, das Sie annehmen oder ablehnen k&ouml;nnen.</p>'
	
}