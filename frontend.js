var mmz_merkzettel;
var mmz_merkzettel_menge;
var outform = '';

$( document ).ready(function() {
	//mmz_get_language();
	mmz_get_storage();
	mmz_get_them_items();

	$('body').append('<div id="mmz_open_merkzettel">');
	$('body').append('<div id="mmz_show_merkzettel">');

	$('#mmz_open_merkzettel').click(function() {
		mmz_show_items('');
	});
	mmz_show_open_merkzettel('show');

})

$(window).bind('resize orientationchange', function() {
	//wub_equalize_them_boxes();
});

//======================================================================
//init and check

function mmz_get_them_items() {
	//document .ready: haengt die funktion an alle h3:
	var i = 0;
	$(mmz_jquery_string).each(function() {

		var iid = $(this).attr('id');
		if (iid == undefined) {
			i++;
			iid = 'mmz_id_'+i;
			$(this).attr('id',iid);
		}

		var titel = mmz_get_title($(this));

		$(this).addClass('mmz_merkzettel_item');
		//$(this).attr('title', mmz_lang['add']);

		if(mmz_merkzettel.hasOwnProperty(titel)){
			$(this).addClass('mmz_selected');
			$(this).attr('title', mmz_lang['remove']);
		}

		$(this).click(function(){
			mmz_add_item($(this));
		});
	});

	mmz_check_status();
}

function mmz_check_status() {
	$(mmz_jquery_string).each(function() {
		var titel = mmz_get_title($(this));
		if(mmz_merkzettel.hasOwnProperty(titel)){
			$(this).addClass('mmz_selected');
			$(this).attr('title', mmz_lang['remove']);
		} else {
			$(this).removeClass('mmz_selected');
			$(this).attr('title', mmz_lang['add']);
		}
	});
	//mmz_show_open_merkzettel('show');
}

//======================================================================
//add, manage and delete items
function mmz_add_item(ths) {

	var titel = mmz_get_title(ths);
	var preis = mmz_get_preis(ths);

	if ($(ths).hasClass('mmz_selected')) {
		delete mmz_merkzettel[titel];
	} else {
		var nr = $(ths).attr('id');
		var url = window.location.href.split('#')[0];
		mmz_merkzettel[titel] = url+'#'+nr;
		if (preis > -1) {mmz_merkzettel_preis[titel] = preis;}

	}
	mmz_check_status();
	mmz_set_storage();
	mmz_show_items();
}

function  mmz_change_menge(ths) {
	var menge = $(ths).val();
	var titel = $(ths).closest('tr').find('a').text();
	mmz_merkzettel_menge[titel] = menge;
	mmz_set_storage();
	mmz_show_items();
	//console.log('mmz_merkzettel_menge: '+JSON.stringify(mmz_merkzettel_menge));
}

function mmz_merkzettel_itemdel(ths) {
	var dings = $(ths).closest('tr').find('a').text();
	//console.log('dings'+dings);
	delete mmz_merkzettel[dings];
	mmz_show_items();
	mmz_check_status();
	mmz_set_storage();
	//reset formfield
	$(mmz_form_field_jquery_string).val(mmz_lang['mein_merkzettel']+ "\n");
}

function mmz_delete_all_items() {
	var really = confirm(mmz_lang['remove_confirm']);
	if (really == true) {
		mmz_merkzettel = {} ;
		mmz_storage_clear();
		mmz_merkzettel_close();
		$('#minimerkzettel h3.mmz_selected').removeClass('mmz_selected');
		mmz_check_status();
	}
}

//======================================================================
//various helpers
function mmz_get_title(ths) {
	var titel = $(ths).attr('data-mmz');
	if (titel == undefined) {
		titel = $(ths).text();
	}
	//console.log(titel);
	return ( page_id +'; '+titel);
}

function mmz_get_preis(ths) {
	var preis = $(ths).attr('data-mmz_preis');
	if (preis == undefined) {
		preis = -1;
	}
	//console.log(titel);
	return parseFloat(preis);
}

//======================================================================
//listing
function mmz_show_items(mode) {
	if (mode == undefined) {mode = '';}

	var out = '';

	//Schleife; alle durch
	var preis_sum_all = 0;
	Object.keys(mmz_merkzettel).forEach(function(key) {

		if (mmz_mengezeigen == true) {
			var menge = mmz_merkzettel_menge[key];
			if (menge == undefined) {
				menge= 1;
			}
			mengetag = '<td><input type="text" value="'+menge+'" onkeyup="mmz_change_menge(this);" /></td>';
		} else {
			mengetag = '';
		}

		var preis_sum = ''; var preistag = '';  var preis_sumtag = ''; var preistag_form ='';
		if (mmz_preiszeigen == true) {
			var preis = mmz_merkzettel_preis[key];
			if (preis == undefined || preis <= 0 || menge < 1) {
				preis = '';
				preis_sum_all = -1;
			} else {
				preistag = mmz_preiswaehrung+' '+preis;
				if (menge > 0) {
					preis_sum = menge * preis;
					preis_sumtag = mmz_preiswaehrung+preis_sum;
					if (preis_sum_all > -1) {preis_sum_all += preis_sum}
				} else {
					//Wenn auch nur 1 Preis fehlt, wird keine Summe gebildet:
					preis_sum_all = -1;
				}
			}
			preistag_form = preistag+ ' | '+preis_sumtag;
			preistag = '<td class="preis">'+preistag+'</td><td class="preis preis_sum">'+preis_sumtag+'</td>';
		}

		var ln = '<tr>'+mengetag+'<td><a href="'+mmz_merkzettel[key]+'">'+key+'</a></td>'+preistag+'<td><span onclick="mmz_merkzettel_itemdel(this);"></span></td></tr>';
		out = ln + out;

		var lnform = 'P'+key;
		if (mmz_mengezeigen == true) {
			lnform = menge+' x '+ lnform;
		}
		if (mmz_preiszeigen == true) {
			lnform += ' | ' + preistag_form;
		}
		lnform = lnform.replace(/&nbsp;/g, ' ' );
		lnform = lnform.replace(/&euro;/g, 'EUR' );
		outform = lnform+ "\n" + outform;


  		//console.log(key, mmz_merkzettel[key]);
	});
	if (preis_sum_all < 0) {preis_sum_all = 'k.A';}

	var mengetag = ''; var mengeth1 = ''; var mengelast = '';
	if (mmz_mengezeigen == true) {
		mengeth1 = '<td>'+mmz_lang['tdmenge']+'</td>';  mengelast = '<td></td>';
	}

	var preistag = ''; var preisth1 = ''; var preislast = '';
	if (mmz_preiszeigen == true) {
		preisth1 = '<td class="preis">'+mmz_lang['tdstk']+'</td><td class="preis preis_sum">'+mmz_lang['tdsum']+'</td>';
		preislast = '<td></td><td class="preis preis_sum">'+mmz_preiswaehrung+preis_sum_all.toFixed(2)+'</td>';
	} else {
		mmz_lang['tdsumtitle'] = '';
	}

	//First and last <tr>
	var th1 = ''; var thlast = ''; var general_hint = '';
	if (mmz_preiszeigen == true) {
		th1 = '<tr class="th1">'+mengeth1+'<td>'+mmz_lang['tdtitel']+'</td>'+preisth1+'<td>&nbsp;</td></tr>';
		if (preis_sum_all > 0) {
			thlast = '<tr class="thlast"><td></td><td style="text-align:right;">'+mmz_lang['tdsumtitle']+'</td>'+preislast+'<td>&nbsp;</td></tr>';
		}
		general_hint =  '<div class="general_hint">'+mmz_lang['general_hint']+'</div>';
	}

	//all output
	if (out == '') {
		out = '<p>'+mmz_lang['dein_merkzettel_leer']+'</p>';
	} else {
		out = '<table>'+th1+out+thlast+'</table><a class="goto_form" href="'+mmz_form_link+'">'+mmz_lang['goto_form']+'</a><p class="remove_all" onclick="mmz_delete_all_items();">'+mmz_lang['remove_all']+'</p>';
	}
	out = '<a class="merkzettel_close" onclick="mmz_merkzettel_close();"></a><h4>'+mmz_lang['dein_merkzettel']+'</h4>' + out;

	if (mode == '') {
		mmz_show_open_merkzettel('hide');
		$('#mmz_show_merkzettel').html(out + general_hint);
		$('#mmz_show_merkzettel').show(500);
	}

	if (mode == 'outform') {
		outform = mmz_lang['mein_merkzettel']+ "\n" +outform;
		return outform;
	}
}

function  mmz_show_open_merkzettel(mode) {
	var keys = Object.keys(mmz_merkzettel);
	if (keys.length < 1) {
		mode = 'hide';
	}
	if (mode == 'hide') {
		$('#mmz_open_merkzettel').hide(0);
	} else {
		$('#mmz_open_merkzettel').show(500);
	}
}

function mmz_merkzettel_close() {
	$('#mmz_show_merkzettel').hide(500);
	mmz_show_open_merkzettel('show');
}

//======================================================================
//connection to form

function mmz_merkzettel_to_form() {
	if (typeof(mmz_form_field_jquery_string) != 'undefined') {
		outform = mmz_show_items('outform');
		$(mmz_form_field_jquery_string).val(outform);
	}
}

//======================================================================
//local (or session) storage:
function mmz_get_storage() {
	mmz_merkzettel = JSON.parse(mmz_storageMethod.getItem('mmz_merkzettel'));
	if (mmz_merkzettel == null) {
		mmz_merkzettel = {} ;
	}

	mmz_merkzettel_menge = JSON.parse(mmz_storageMethod.getItem('mmz_merkzettel_menge'));
	if (mmz_merkzettel_menge == null) {
		mmz_merkzettel_menge = {} ;
	}

	mmz_merkzettel_preis = JSON.parse(mmz_storageMethod.getItem('mmz_merkzettel_preis'));
	if (mmz_merkzettel_preis == null) {
		mmz_merkzettel_preis = {} ;
	}
	//console.log('mmz_merkzettel_menge: '+JSON.stringify(mmz_merkzettel_menge));
	mmz_merkzettel_to_form();
}

function mmz_set_storage() {
	mmz_storageMethod.setItem('mmz_merkzettel', JSON.stringify(mmz_merkzettel));
	mmz_storageMethod.setItem('mmz_merkzettel_menge', JSON.stringify(mmz_merkzettel_menge));
	mmz_storageMethod.setItem('mmz_merkzettel_preis', JSON.stringify(mmz_merkzettel_preis));
	//console.log('mmz_merkzettel_menge: '+JSON.stringify(mmz_merkzettel_menge));
}

function mmz_storage_clear() {
	mmz_storageMethod.setItem('mmz_merkzettel', null);
	//Menge wird NICHT geloescht!
}

//=====================================
function mmz_get_language() {
}
