<?php
// Must include em to stop this file being access directly
if(defined('WB_PATH') == false) { exit("Cannot access this file directly"); }
$df = dirname(__FILE__);
include $df.'/module_config.php';
$mod_dir = basename($df);
?>

<div class="mmz_merkzettel">
<div class="mmz_open_merkzettel"></div>

<?php if (LANGUAGE == 'DE') { ?>
	<h2>Merkzettel aktiviert</h2>
	<p>Die Funktion wird an jedes Element <em>$('<?php echo $jquery_string; ?>')</em> angeh&auml;ngt. Die Einstellung 'preiszeigen' steht auf <em><?php echo $preiszeigen; ?></em>. </p>

	<?php if ($preiszeigen == 'true') { echo "<p>Die Währung ist: <em>$preiswaehrung</em>. Als Preisangabe m&uuml;ssen die Elemente enthalten (zb): <em>data-mmz_preis=99.99</em> (Punkt statt Komma!).</p>";} ?>

	<p><b>Anfrage-Formular</b>: Als $page_id der Formularseite ist <em><?php echo $form_field_page_id; ?></em> angegeben. Diese Seite MUSS ein Formular UND einen Merkzettel-Abschnitt enthalten. </p>
	<p>Das Ziel-Element im Formular ist mit <em>$('<?php echo $form_field_jquery_string; ?>')</em> angegeben.</p>
	<p>Einstellungen können in: <em><?php echo '/modules/'.$mod_dir.'/module_config.php'; ?></em><b> geändert werden. | <a target="_blank" href="<?php echo WB_URL.'/modules/'.$mod_dir.'/languages/help-'.LANGUAGE.'.html'; ?>">Hilfe</a></b>
	</p>
<?php } else { ?>
	<h2>Merkzettel (wish list) is active</h2>
	<p>The funktion will be added to each element <em>$('<?php echo $jquery_string; ?>')</em>. The setting 'preiszeigen' (show prices) is <em><?php echo $preiszeigen; ?></em>. <br />

	<?php if ($preiszeigen == 'true') { echo "currency-string is: <em>$preiswaehrung</em>. The elements must contain (eg): <em>data-mmz_preis=99.99</em> (Dot instead of Komma!) to handle the value.<br />";}  ?>
	</p><p>
	<b>Inquiry Form</b>: The $page_id of the form-page is set to <em><?php echo $form_field_page_id; ?></em>. This page MUST contain a form AND a Merkzettel-section. <br />
	The target element of the form is set to <em>$('<?php echo $form_field_jquery_string; ?>')</em>.
	</p><p>Change settings in: <em><?php echo '/modules/'.$mod_dir.'/module_config.php'; ?></em><b> | <a target="_blank" href="<?php echo WB_URL.'/modules/'.$mod_dir.'/languages/help-EN.html'; ?>">Help</a></b>
	</p>


<?php }?>


</div>
