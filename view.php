<?php
$df = dirname(__FILE__);
include $df.'/module_config.php';
$mod_dir = basename($df);

$f = $df.'/languages/'.LANGUAGE.'.js';
if (file_exists($f)) {$jslang = LANGUAGE; } else  {$jslang = 'EN'; }

echo '
<script src="'.WB_URL. '/modules/'.$mod_dir.'/languages/'.$jslang.'.js" type="text/javascript"></script>
';

/*
DEFINE('MINIMERKZETTEL', true);
echo '<div id="minimerkzettel">
*/

echo '
<script>
var page_id='.PAGE_ID.';
var mmz_jquery_string="'.$jquery_string.'";
var mmz_preiszeigen = '.$preiszeigen.';
var mmz_preiswaehrung = "'.$preiswaehrung.'";

var mmz_mengezeigen = '.$mengezeigen.';
var mmz_form_link = "[wblink'.$form_field_page_id.']";
';

$storage_method = ($use_session_storage == 'true') ? 'sessionStorage' : 'localStorage';
echo 'var mmz_storageMethod = '.$storage_method.';
';

if (PAGE_ID == $form_field_page_id) {echo '
var mmz_form_field_jquery_string = "'.$form_field_jquery_string.'";
'; }

echo '
</script>
';


?>
